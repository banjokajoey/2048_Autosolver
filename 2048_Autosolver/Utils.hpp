//
//  Utils.hpp
//  2048_Autosolver
//
//  Created by Joseph Hernandez on 6/11/17.
//  Copyright © 2017 Joseph Hernandez. All rights reserved.
//

#ifndef Utils_h
#define Utils_h

namespace Utils
{
    float randFloat();
}

#endif /* Utils_h */
