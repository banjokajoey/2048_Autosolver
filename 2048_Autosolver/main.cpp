//
//  main.cpp
//  2048_Autosolver
//
//  Created by Joseph Hernandez on 6/11/17.
//  Copyright © 2017 Joseph Hernandez. All rights reserved.
//

#include <iostream>
#include "Game.hpp"
#include "Solver.hpp"


Move getMove()
{
    char ch;
    for (int i = 5; i >= 1; --i)
    {
        std::cout << i << " tries to make a move (wasd): ";
        std::cin >> ch;
        switch (ch)
        {
            case 'W': case 'w':
                return UP;
            case 'A': case 'a':
                return LEFT;
            case 'S': case 's':
                return DOWN;
            case 'D': case 'd':
                return RIGHT;
        }
    }
    return NONE;
}


void manualPlay(Game *game)
{
    std::cout << std::endl << "Manual play:" << std::endl;
    while (!game->gameIsOver())
    {
        std::cout << std::endl << *game << std::endl;
        Move playerMove = getMove();
        game->step(playerMove);
    }
}


void autoPlay(Game *game, Solver *solver)
{
    solver->playGame(game);
}


int main(int argc, const char * argv[]) {
    std::cout << "========================================" << std::endl;
    std::cout << " 2048 Autosolver" << std::endl;
    std::cout << "----------------------------------------" << std::endl;
    
    std::cout << "Setting up game..." << std::endl;
    Game *game = new Game();
    
    //manualPlay(game);
    autoPlay(game, new RandomSolver());
    
    std::cout << std::endl;
    if (game->gameIsWon())
    {
        std::cout << "GAME WON!" << std::endl;
    }
    else
    {
        std::cout << "GAME OVER" << std::endl;
    }
    std::cout << "max val   = " << game->getMaxValue() << std::endl;
    std::cout << "num steps = " << game->getNumSteps() << std::endl;
    
    return 0;
}
