//
//  Solver.hpp
//  2048_Autosolver
//
//  Created by Joseph Hernandez on 6/11/17.
//  Copyright © 2017 Joseph Hernandez. All rights reserved.
//

#ifndef Solver_hpp
#define Solver_hpp

class Game;

class Solver
{
public:
    Solver();
    ~Solver();
    virtual void playGame(Game *game) = 0;
};


class RandomSolver : public Solver
{
public:
    RandomSolver();
    ~RandomSolver();
    void playGame(Game *game);
};

#endif /* Solver_hpp */
