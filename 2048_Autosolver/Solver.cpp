//
//  Solver.cpp
//  2048_Autosolver
//
//  Created by Joseph Hernandez on 6/11/17.
//  Copyright © 2017 Joseph Hernandez. All rights reserved.
//

#include "Solver.hpp"
#include "Game.hpp"
#include "Utils.hpp"
#include <iostream>


Solver::Solver()
{
    
}


Solver::~Solver()
{
    
}


RandomSolver::RandomSolver()
{
    
}


RandomSolver::~RandomSolver()
{
    
}


void RandomSolver::playGame(Game *game)
{
    std::cout << *game << std::endl << std::endl;
    while (!game->gameIsOver())
    {
        Move guess = (Move)(int)(Utils::randFloat() * 4);
        game->step(guess);
        std::cout << *game << std::endl;
    }
}
