//
//  Utils.cpp
//  2048_Autosolver
//
//  Created by Joseph Hernandez on 6/11/17.
//  Copyright © 2017 Joseph Hernandez. All rights reserved.
//

#include "Utils.hpp"
#include <stdlib.h>

namespace Utils
{
    float randFloat()
    {
        return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    }
}
