//
//  Game.cpp
//  2048_Autosolver
//
//  Created by Joseph Hernandez on 6/11/17.
//  Copyright © 2017 Joseph Hernandez. All rights reserved.
//

#include "Game.hpp"
#include <cmath>
#include <iostream>
#include "Utils.hpp"


Game::Game()
: isOver(false)
, maxValue(0)
, numSteps(0)
{
    // Initializa 2D game grid
    grid = new int*[GRID_DIM];
    for (int i = 0; i < GRID_DIM; ++i)
    {
        int *row = new int[GRID_DIM];
        for (int j = 0; j < GRID_DIM; ++j)
        {
            row[j] = 0;
        }
        grid[i] = row;
    }
    
    // Initialize board config
    for (int i = 0; i < NUM_START_CELLS; ++i)
    {
        this->newCell();
    }
}


Game::~Game()
{
    // Deallocate game grid
    for (int i = 0; i < GRID_DIM; ++i)
    {
        delete[] grid[i];
    }
    delete[] grid;
}


int Game::getValue(int row, int col) const
{
    if (Game::coordsAreInBounds(row, col))
    {
        return grid[row][col];
    }
    return -1;
}


void Game::setValue(int row, int col, int val)
{
    if (Game::coordsAreInBounds(row, col))
    {
        grid[row][col] = val;
    }
}


int Game::getMaxValue() const
{
    return maxValue;
}


int Game::getNumSteps() const
{
    return numSteps;
}


int Game::getNumCells() const
{
    int count = 0;
    for (int row = 0; row < GRID_DIM; ++row)
    {
        for (int col = 0; col < GRID_DIM; ++col)
        {
            if (this->getValue(row, col) != 0)
            {
                ++count;
            }
        }
    }
    return count;
}


void Game::gameOver()
{
    isOver = true;
}


void Game::step(Move playerMove)
{
    if (playerMove == NONE)
    {
        return;
    }
    
    bool swipeSuccess = this->swipe(playerMove);
    if (swipeSuccess)
    {
        ++numSteps;
        if (this->gameIsWon())
        {
            this->gameOver();
        }
        else
        {
            this->newCell();
        }
    }
    
    if (!moveIsPossible())
    {
        this->gameOver();
    }
}


bool Game::gameIsOver()
{
    return isOver;
}


bool Game::gameIsWon()
{
    for (int row = 0; row < GRID_DIM; ++row)
    {
        for (int col = 0; col < GRID_DIM; ++col)
        {
            if (this->getValue(row, col) == 2048)
            {
                return true;
            }
        }
    }
    return false;
}


bool Game::swipe(Move playerMove)
{
    if (playerMove == NONE)
    {
        return false;
    }
    
    bool moveOccurred = false;
    if (playerMove == UP || playerMove == DOWN)
    {
        moveOccurred |= this->slideVertically(playerMove == UP);
        moveOccurred |= this->mergeVertically();
        moveOccurred |= this->slideVertically(playerMove == UP);
    }
    else // then playerMove == LEFT || playerMove == RIGHT
    {
        moveOccurred |= this->slideHorizontally(playerMove == LEFT);
        moveOccurred |= this->mergeHorizontally();
        moveOccurred |= this->slideHorizontally(playerMove == LEFT);
    }
    
    return moveOccurred;
}


bool Game::mergeVertically()
{
    bool mergeOccurred = false;
    for (int col = 0; col < GRID_DIM; ++col)
    {
        for (int row = 0; row < GRID_DIM - 1; ++row)
        {
            int val1 = this->getValue(row, col);
            int val2 = this->getValue(row + 1, col);
            if (val1 != 0 && val1 == val2)
            {
                int newVal = val1 + val2;
                if (newVal > maxValue)
                {
                    maxValue = newVal;
                }
                this->setValue(row, col, newVal);
                this->setValue(row + 1, col, 0);
                mergeOccurred = true;
            }
        }
    }
    return mergeOccurred;
}


bool Game::mergeHorizontally()
{
    bool mergeOccurred = false;
    for (int row = 0; row < GRID_DIM; ++row)
    {
        for (int col = 0; col < GRID_DIM - 1; ++col)
        {
            int val1 = this->getValue(row, col);
            int val2 = this->getValue(row, col + 1);
            if (val1 != 0 && val1 == val2)
            {
                int newVal = val1 + val2;
                if (newVal > maxValue)
                {
                    maxValue = newVal;
                }
                this->setValue(row, col, newVal);
                this->setValue(row, col + 1, 0);
                mergeOccurred = true;
            }
        }
    }
    return mergeOccurred;
}


bool Game::slideVertically(bool slideUp)
{
    bool slideOccurred = false;
    int startRow = (slideUp) ? 0 : (GRID_DIM - 1);
    int endRow = (slideUp) ? (GRID_DIM - 1) : 0;
    int rowIncr = (slideUp) ? 1 : -1;
    for (int col = 0; col < GRID_DIM; ++col)
    {
        int nextOpenRow = startRow;
        for (int row = startRow; row != endRow + rowIncr; row += rowIncr)
        {
            int val = this->getValue(row, col);
            if (val != 0)
            {
                if (row != nextOpenRow)
                {
                    this->setValue(nextOpenRow, col, val);
                    this->setValue(row, col, 0);
                    slideOccurred = true;
                }
                nextOpenRow += rowIncr;
            }
        }
    }
    return slideOccurred;
}


bool Game::slideHorizontally(bool slideLeft)
{
    bool slideOccurred = false;
    int startCol = (slideLeft) ? 0 : (GRID_DIM - 1);
    int endCol = (slideLeft) ? (GRID_DIM - 1) : 0;
    int colIncr = (slideLeft) ? 1 : -1;
    for (int row = 0; row < GRID_DIM; ++row)
    {
        int nextOpenCol = startCol;
        for (int col = startCol; col != endCol + colIncr; col += colIncr)
        {
            int val = this->getValue(row, col);
            if (val != 0)
            {
                if (col != nextOpenCol)
                {
                    this->setValue(row, nextOpenCol, val);
                    this->setValue(row, col, 0);
                    slideOccurred = true;
                }
                nextOpenCol += colIncr;
            }
        }
    }
    return slideOccurred;
}


bool Game::moveIsPossible()
{
    if (this->getNumFreeCells() > 0)
    {
        return true;
    }
    
    for (int row = 0; row < GRID_DIM - 1; ++row)
    {
        for (int col = 0; col < GRID_DIM - 1; ++col)
        {
            if (this->getValue(row, col) == this->getValue(row, col + 1)
                || this->getValue(row, col) == this->getValue(row + 1, col))
            {
                return true;
            }
        }
    }
    return false;
}


void Game::newCell()
{
    int numFree = this->getNumFreeCells();
    if (numFree == 0)
    {
        return;
    }
        
    int newValue = (Utils::randFloat() < 0.9) ? 2 : 4;
    int freeSeen = 0;
    int targetFree = Utils::randFloat() * numFree;
    
    for (int i = 0; i < GRID_DIM; ++i)
    {
        for (int j = 0; j < GRID_DIM; ++j)
        {
            if (this->getValue(i, j) == 0)
            {
                if (freeSeen == targetFree)
                {
                    this->setValue(i, j, newValue);
                    return;
                }
                else
                {
                    ++freeSeen;
                }
            }
        }
    }
}


int Game::getNumFreeCells() const
{
    return std::max(GRID_DIM * GRID_DIM - this->getNumCells(), 0);
}


bool Game::coordsAreInBounds(int row, int col)
{
    return row >= 0 && row < GRID_DIM && col >= 0 && col < GRID_DIM;
}


std::ostream& operator << (std::ostream& out, const Game::Game& game)
{
    out << "STEP " << game.getNumSteps() << ':' << std::endl;
    for (int row = 0; row < Game::GRID_DIM; ++row)
    {
        for (int col = 0; col < Game::GRID_DIM; ++col)
        {
            int val = game.getValue(row, col);
            if (val == 0)
            {
                out << "----";
            }
            else
            {
                for (int i = 0; i < 4; ++i)
                {
                    if (val < std::pow(10, i))
                    {
                        out << '-';
                    }
                }
                out << val;
            }
            out << " ";
        }
        out << std::endl;
    }
    return out;
}
