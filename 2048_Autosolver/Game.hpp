//
//  Game.hpp
//  2048_Autosolver
//
//  Created by Joseph Hernandez on 6/11/17.
//  Copyright © 2017 Joseph Hernandez. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include <iosfwd>


// Move enum
enum Move
{
    UP = 0,
    RIGHT,
    DOWN,
    LEFT,
    NONE
};


class Game
{
public:
    // Constants
    static const int GRID_DIM = 4;
    static const int NUM_START_CELLS = 2;
    
    // Constructor/Destructor
    Game();
    ~Game();
    
    // Accessors
    int getValue(int row, int col) const;
    void setValue(int row, int col, int val);
    int getMaxValue() const;
    int getNumSteps() const;
    int getNumCells() const;
    int getNumFreeCells() const;
    
    // Methods
    void gameOver();
    void step(Move playerMove);
    bool gameIsOver();
    bool gameIsWon();
    
private:
    
    // Private methods
    bool swipe(Move playerMove);
    bool mergeVertically();
    bool mergeHorizontally();
    bool slideVertically(bool slideUp);
    bool slideHorizontally(bool slideLeft);
    void newCell();
    bool moveIsPossible();
    
    // Private static helpers
    static bool coordsAreInBounds(int row, int col);
    
    // Object variables
    int** grid;
    bool isOver;
    int maxValue;
    int numSteps;
};

// Operator overloads
std::ostream& operator << (std::ostream& os, const Game::Game& game);

#endif /* Game_hpp */
